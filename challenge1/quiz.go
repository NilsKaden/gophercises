package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"
)

/* bonus points:
Add string trimming and cleanup to help ensure that correct answers with extra whitespace, capitalization, etc are not considered incorrect. Hint: Check out the strings package.
Add an option (a new flag) to shuffle the quiz order each time it is run.
*/

var questionsCount int
var correctAnswerCount int

func updateCounters(userAnsweredCorrectly bool) {
	questionsCount++

	if userAnsweredCorrectly == true {
		correctAnswerCount++
	}
}

func getAndParseFile() {
	var filePath = flag.String("path", "problems.csv", "relative file path")
	flag.Parse()

	file, err := os.Open(*filePath)
	if err != nil {
		log.Fatal(err)
	}

	r := csv.NewReader(file)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		var userAnsweredCorrectly = askQuestion(record[0], record[1]) // assumes data to always be correct
		updateCounters(userAnsweredCorrectly)
	}

	// quiz is done
	fmt.Println(fmt.Sprintf("Questions asked:%d\nCorrect answers:%d", questionsCount, correctAnswerCount))
}

func askQuestion(question string, expectedAnswer string) bool {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println(question)
	text, _ := reader.ReadString('\n') // we gotta trim the string here
	trimmedText := strings.Trim(text, "\n")

	answeredCorrectly := strings.Compare(strings.ToLower(trimmedText), strings.ToLower(expectedAnswer))

	if answeredCorrectly == 0 {
		return true
	}
	return false
}

// channels and goroutines for the timer part
func timer() {
	timer := time.NewTimer(30 * time.Second)
	fmt.Println("timer started!")

	<-timer.C // gets executed once timer has expired
	fmt.Println(fmt.Sprintf("TIMER EXPIRED!\nQuestions asked:%d\nCorrect answers:%d", questionsCount, correctAnswerCount))
}

func main() {
	go timer() // start timer goroutine in parallel
	getAndParseFile()
}
